<?php

return [
    'site_status' => [
        'status' => [
            'active' => 'Active', 
            'inactive' => 'Inactive'
        ],
        'default_checked' => 'active'
    ],
    'site_type' => [
        'development' => 'Development',
        'live' => 'Live'
    ],
    'site_host' => [
        'ecomitize' => 'Ecomitize',
        'Flywheel' => 'Flywheel',
        'Wpengine' => 'Wpengine',
        'LIQUIDWEB' => 'LIQUIDWEB'
    ],
];
