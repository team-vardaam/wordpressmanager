<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>Wordpress Sites Manager</title>
        <meta name="description" content="Wordpress Sites Manager">
        <meta name="author" content="Vardaam">
        <meta name="robots" content="noindex, nofollow">
        <!-- Open Graph Meta -->
        <meta property="og:title" content="">
        <meta property="og:site_name" content="Wordpress Sites Manager">
        <meta property="og:description" content="">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">
        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ asset('backoffice-asset/media/favicons/favicon.png') }}">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ asset('backoffice-asset/media/favicons/favicon-192x192.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/media/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->
        <!-- Stylesheets -->
        <!-- Fonts and Dashmix framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap">
        <link rel="stylesheet" id="css-main" href="{{ asset('backoffice-asset/css/dashmix.min.css') }}">
        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/xwork.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
        <div id="app">
            <div id="page-container">
                <!-- Main Container -->
                <main id="main-container">
                    <!-- Page Content -->
                    <div class="row no-gutters justify-content-center bg-body-dark">
                        <div class="hero-static col-sm-10 col-md-8 col-xl-6 d-flex align-items-center p-2 px-sm-0">
                            <!-- Sign Up Block -->
                            <div class="block block-rounded block-transparent block-fx-pop w-100 mb-0 overflow-hidden bg-image" style="background-image: url({{ asset('backoffice-asset/media/photos/photo20@2x.jpg') }});">
                                @yield('content')
                            </div>
                            <!-- END Sign Up Block -->
                        </div>
                    </div>
                    <!-- END Page Content -->
                </main>
                <!-- END Main Container -->
            </div>
        </div>
        <script src="{{ asset('backoffice-asset/js/dashmix.app.js') }}"></script>
        <!-- Page JS Plugins -->
        <script src="{{ asset('backoffice-asset/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    </body>
</html>