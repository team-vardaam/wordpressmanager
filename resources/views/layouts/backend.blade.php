<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>

        <meta name="robots" content="noindex, nofollow">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <!-- Icons -->
        <link rel="shortcut icon" href="{{ asset('backoffice-asset/media/favicons/favicon.png') }}">
        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('backoffice-asset/media/favicons/favicon-192x192.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('backoffice-asset/media/favicons/apple-touch-icon-180x180.png') }}">

        @stack('styles')

        <!-- Fonts and Styles -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700&display=swap">
        <link rel="stylesheet" id="css-main" href="{{ asset('backoffice-asset/css/dashmix.css') }}">

        <!-- You can include a specific file from public/css/themes/ folder to alter the default color theme of the template. eg: -->
        <link rel="stylesheet" id="css-theme" href="{{ asset('backoffice-asset/css/themes/xeco.css') }}">
       
        <!-- Sweet alert  -->
        <link rel="stylesheet" id="css-theme" href="{{ asset('backoffice-asset/js/plugins/sweetalert2/sweetalert2.min.css') }}">

        <!-- SweetAlert CSS-->
        <link rel="stylesheet" href="{{ asset('backoffice-asset/js/plugins/sweetalert2/sweetalert2.min.css') }}">
        <!-- ! SweetAlert CSS-->

        <!-- iCheck -->
        <link rel="stylesheet" href="{{asset('backoffice-asset/js/plugins/iCheck/square/green.css')}}">

        <link rel="stylesheet" id="css-theme" href="{{ asset('css/custom.css') }}">
        <!-- Scripts -->
        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
    </head>
    <body>
        <div id="page-container" class="enable-page-overlay side-scroll side-trans-enabled sidebar-o page-header-dark sidebar-dark">
            @include('layouts.partials.backend_sidebar')

            @include('layouts.partials.backend_header')

            <!-- Main Container -->
            <main id="main-container">
                @include('layouts/partials.backend_hero')

                <div class="content">
                    @yield('content')
                </div>
            </main>
            <!-- END Main Container -->

            @include('layouts.partials.backend_footer')
        </div>
        <!-- END Page Container -->

        <!-- Dashmix Core JS -->
        <script src="{{ asset('backoffice-asset/js/dashmix.app.js') }}"></script>

        <!-- SweetAlert JS-->
        <script src="{{ asset('backoffice-asset/js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

        <!-- ! SweetAlert JS-->
        <script src="{{ asset('backoffice-asset/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('backoffice-asset/js/plugins/iCheck/icheck.min.js') }}"></script>
        <script src="{{ asset('js/custom.js') }}"></script>
        <script type="text/javascript">
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        @stack('scripts')
    </body>
</html>
