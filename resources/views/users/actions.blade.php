<div class="btn-toolbar">
    <div class="btn-group">
        <a href="{{ route('users.change-password', $users->id) }}" class="btn btn-sm btn-alt-primary js-tooltip-enabled" data-toggle="tooltip" title="Change Password" data-original-title="Change password"><i class="fa fa-key"></i>&nbsp; Change Password</a>
    </div>
</div>