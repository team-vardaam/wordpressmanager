@extends('layouts.backend')

@section('title', 'List Users')

@push('styles')
<link rel="stylesheet" href="{{ asset('backoffice-asset/js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush

@section('content')
<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title">User List</h3>
    </div>
    <div class="block-content block-content-full table-responsive">
        <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm" id="users-table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Member Since</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('backoffice-asset/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backoffice-asset/js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    var dTable = '';
    jQuery(function() {
        loadPartialDataTable(role = '')     

        function loadPartialDataTable(role = '') {
            dTable = jQuery('#users-table').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: !1,
                order: [[ 1, "asc" ]],
                language: {
                    searchPlaceholder: "Search by email or name.."
                },
                ajax: {
                    url: "{{ route('users.data') }}",
                    type: "POST"
                },
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'actions', name: 'actions', orderable: false, searchable: false, sClass : 'text-center' },
                ]   
            });
        }
    });

    function loadDataTable() {
        dTable.ajax.reload( null, false );
    }
</script>
@endpush