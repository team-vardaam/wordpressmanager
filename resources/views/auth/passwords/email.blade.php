@extends('layouts.app')

@section('content')
<div class="row no-gutters">
    <div class="col-md-6 order-md-1 bg-white">
        <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
            <!-- Header -->
            <div class="mb-2 text-center">
                <a class="link-fx text-warning font-w700 font-size-h2" href="index.html">
                    <span class="text-dark">Wordpress </span><span class="text-primary">Sites Manager</span>
                </a>
                <p class="text-uppercase font-w700 font-size-sm text-muted">Password Reminder</p>
            </div>
            <!-- END Header -->

            <!-- Reminder Form -->
            <!-- jQuery Validation (.js-validation-reminder class is initialized in js/pages/op_auth_reminder.min.js which was auto compiled from _js/pages/op_auth_reminder.js) -->
            <!-- For more info and examples you can check out https://github.com/jzaefferer/jquery-validation -->
            <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <x-auth-validation-errors class="mb-4" :errors="$errors" />
                <div class="form-group">
                    <input id="email" type="email" class="form-control form-control-alt" name="email" value="{{ $email ?? old('email') }}" placeholder="Email address" autocomplete="email" autofocus>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-block btn-hero-primary">
                        <i class="fa fa-fw fa-reply mr-1"></i> Send Password Reset Link
                    </button>
                </div>
            </form>
            <!-- END Reminder Form -->
        </div>
    </div>
    <div class="col-md-6 order-md-0 bg-primary-dark-op d-flex align-items-center">
        <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6 text-center">
            <p class="font-size-h2 font-w700 text-white mb-0">
                Don’t worry of failure..
            </p>
            <p class="font-size-h3 font-w600 text-white-75 mb-0">
                ..but learn from it!
            </p>
        </div>
    </div>
</div>
@endsection