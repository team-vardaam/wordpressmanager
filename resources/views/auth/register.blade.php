@extends('layouts.app')

@section('content')
<div class="row no-gutters">
    <div class="col-md-6 order-md-1 bg-white">
        <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
            <!-- Header -->
            <div class="mb-2 text-center">
                <a class="link-fx text-success font-w700 font-size-h2" href="index.html">
                <span class="text-dark">Wordpress </span><span class="text-primary">Sites Manager</span>
                </a>
                <p class="text-uppercase font-w700 font-size-sm text-muted">Create New Account</p>
            </div>
            <!-- END Header -->
            <!-- Sign Up Form -->
            <!-- jQuery Validation (.js-validation-signup class is initialized in js/pages/op_auth_signup.min.js which was auto compiled from _js/pages/op_auth_signup.js) -->
            <!-- For more info and examples you can check out https://github.com/jzaefferer/jquery-validation -->
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                <div class="form-group">    
                    <input id="name" type="text" class="form-control form-control-alt" name="name" value="{{ old('name') }}"  autocomplete="name" autofocus placeholder="Name">
                </div>
                <div class="form-group">
                    <input id="email" type="email" class="form-control form-control-alt" name="email" value="{{ old('email') }}" placeholder="Email" autocomplete="email">
                </div>
                <div class="form-group">
                    <input id="password" type="password" class="form-control form-control-alt" name="password" placeholder="Password" autocomplete="new-password">
                </div>
                <div class="form-group">
                    <input id="password-confirm" type="password" class="form-control form-control-alt" name="password_confirmation"      autocomplete="new-password" placeholder="Password Confirm">
                </div>
                <div class="form-group mb-3">
                    <div class="col-sm-12">  
                        <a href="{{ route('login') }}" class="float-right text-muted mb-3">Already registered ?</a>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-hero-primary">
                    <i class="fa fa-fw fa-plus mr-1"></i> Sign Up
                    </button>
                </div>
            </form>
            <!-- END Sign Up Form -->
        </div>
    </div>
    <div class="col-md-6 order-md-0 bg-primary-op d-flex align-items-center">
        <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
            <div class="media">
                <a class="img-link mr-3" href="javascript:void(0)">
                <img class="img-avatar img-avatar-thumb" src="{{ asset('backoffice-asset/media/avatars/avatar7.jpg') }}" alt="">
                </a>
                <div class="media-body">
                    <p class="text-white font-w600 mb-1">
                        The service was a valuable asset to improve our conversions and marketing efforts! Thank you!
                    </p>
                    <a class="text-white-75 font-w600" href="javascript:void(0)">Sara Fields, CEO</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection