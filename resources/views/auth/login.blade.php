@extends('layouts.app')

@section('content')
<div class="row no-gutters">
    <div class="col-md-6 order-md-1 bg-white">
        <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
            <!-- Header -->
            <div class="mb-2 text-center">
                <a class="link-fx font-w700 font-size-h2" href="{{ route('login') }}">
                <span class="text-dark">Wordpress </span><span class="text-primary">Sites Manager</span>
                </a>
                <p class="text-uppercase font-w700 font-size-sm text-muted">Sign In</p>
            </div>
            <!-- END Header -->
            <!-- Sign In Form -->
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                <div class="form-group">
                    <div> {!! Form::email('email', '', ['placeholder' => '', 'class' => 'form-control form-control-alt', 'id' => 'email']); !!}</div>
                </div>
                <div class="form-group">
                    <div>
                        <x-input id="password" class="form-control" type="password" name="password"  autocomplete="new-password" />
                    </div>
                </div>
                <div class="form-group mb-3">
                    <div class="col-sm-12">  <input id="remember_me" type="checkbox" class="form-check-input" name="remember">
                        <label for="remember_me" class="form-check-label text-sm">
                        {{ __('Remember me') }}
                        </label>
                        <a href="{{ route('register') }}" class="float-right text-muted ">Not registered yet ?</a>
                        <br/>
                        @if (Route::has('password.request'))
                        <a class="text-muted float-right mb-3" href="{{ route('password.request') }}">
                        {{ __('Forgot your password ?') }}
                        </a>
                        @endif
                    </div>
                </div>
                <div class="form-group mt-4">
                    <button type="submit" class="btn btn-block btn-hero-primary">
                    <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Sign In
                    </button>
                </div>
            </form>
            <!-- END Sign In Form -->
        </div>
    </div>
    <div class="col-md-6 order-md-0 bg-primary-dark-op d-flex align-items-center">
        <div class="block-content block-content-full px-lg-5 py-md-5 py-lg-6">
            <div class="media">
                <a class="img-link mr-3" href="javascript:void(0)">
                <img class="img-avatar img-avatar-thumb" src="{{ asset('backoffice-asset/media/avatars/avatar12.jpg') }}" alt="">
                </a>
                <div class="media-body">
                    <p class="text-white font-w600 mb-1">
                        Amazing framework with tons of options! It helped us build our project!
                    </p>
                    <a class="text-white-75 font-w600" href="javascript:void(0)">Jesse Fisher, Web Developer</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection