<div class="btn-toolbar">
    <div class="btn-group">
        <a href="{{ route('sites.change-password',['site_id' => $id, 'user_id' => $users->data->ID] )}}" class="btn btn-sm btn-alt-primary js-tooltip-enabled" data-toggle="tooltip" title="Change Password" data-original-title="Change Password"><i class="fa fa-edit"></i>&nbsp; Change Password</a>&nbsp;
        @if( $users->user_role != 'administrator')
        <a href="{{ route('sites.delete_user',['site_id' => $id, 'user_id' => $users->data->ID] )}}" class="btn btn-sm btn-alt-danger btn-delete" data-toggle="tooltip" title="Delete" data-original-title="Delete"><i class="fa fa-trash-alt"></i>&nbsp;Delete User</a>
        @endif
    </div>
</div>