@extends('layouts.backend')

@section('title', ucfirst($title))

@push('styles')
<link rel="stylesheet" href="{{ asset('backoffice-asset/js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush

@section('content')
<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title">Users List</h3>
    </div>
    <div class="block-content block-content-full table-responsive">
        <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm yajra-datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Display Name</th>
                    <th>User Login</th>
                    <th>User Email</th>
                    <th>User URL</th>
                    <th>User Role</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('backoffice-asset/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backoffice-asset/js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    var dTable = '';
    jQuery(function() {
        loadPartialDataTable();

        function loadPartialDataTable() {
            dTable = jQuery('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: !1,
                order: [[ 0, "desc" ]],
                language: {
                    searchPlaceholder: "Search here"
                },
                ajax: {
                    url: "{{ route('sites.user_index_data', $id) }}",
                    type: "POST"
                },
                columns: [
                    { data: 'ID', name: 'ID' },
                    { data: 'display_name', name: 'display_name' },
                    { data: 'user_login', name: 'user_login' },
                    { data: 'user_email', name: 'user_email' },
                    { data: 'user_url', name: 'user_url' },
                    { data: 'user_role', name: 'user_role' },
                    { data: 'actions', name: 'actions', orderable: false, searchable: false},
                ]   
            });
        }
    });

    function loadDataTable() {
        dTable.ajax.reload( null, false );
    }
</script>
@endpush