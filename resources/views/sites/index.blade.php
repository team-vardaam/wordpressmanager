@extends('layouts.backend')

@section('title', 'Manage Sites')

@push('styles')
<link rel="stylesheet" href="{{ asset('backoffice-asset/js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush

@section('content')
<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title">List Sites</h3>
    </div>
    <div class="block-content block-content-full table-responsive">
    <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm yajra-datatable">
            <thead>
                <tr class="text-uppercase">
                    <th scope="col">#</th>
                    <th scope="col">Site Title</th>
                    <th scope="col">Site Url</th>
                    <th scope="col">Admin Name</th>
                    <th scope="col">Type</th>
                    <th scope="col">Host</th>
                    <th scope="col">Note</th>
                    <th scope="col">Status</th>
                    <th scope="col"></th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('backoffice-asset/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backoffice-asset/js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    var dTable = '';
    jQuery(function() {
        loadPartialDataTable(role = '')     

        function loadPartialDataTable(role = '') {
            dTable = jQuery('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: !1,
                order: [[ 1, "desc" ]],
                language: {
                    searchPlaceholder: "Search here"
                },
                ajax: {
                    url: "{{ route('sites.data') }}",
                    type: "POST"
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'title', name: 'title' },
                    { data: 'url', name: 'url' },
                    { data: 'admin', name: 'admin' },
                    { data: 'type', name: 'type' },
                    { data: 'host', name: 'host' },
                    { data: 'note', name: 'note' },
                    { data: 'status', name: 'status' },
                    { data: 'plugin_status', name: 'plugin_status' },
                    { data: 'actions', name: 'actions', orderable: false, searchable: false},

                ]   
            });
        }
    });

    function loadDataTable() {
        dTable.ajax.reload( null, false );
    }
</script>
@endpush