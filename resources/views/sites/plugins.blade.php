@extends('layouts.backend')
@section('title', ucfirst($title))
@section('content')
<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title">Plugin List</h3>
    </div>
    <div class="block-content block-content-full table-responsive">
        <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm">
            <thead>
                <tr class="text-uppercase">
                    <th scope="col">Plugin Name</th>
                    <th scope="col">Plugin URI</th>
                    <th scope="col">Author Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Status</th>
                    <th scope="col">Installed Version</th>
                    <th scope="col">Latest Version</th>
                </tr>
            </thead>
            <tbody>
                @if(!empty($plugin_list))
                @foreach($plugin_list as $key=>$value)
                <tr>
                    <td>{{ $value->Name }}</td>
                    <td>{{ $value->PluginURI }}</td>
                    <td>{{ $value->AuthorName }}</td>
                    <td>{!! $value->Description !!}</td>
                    @if($value->Status == 'Active')
                    <td><button type="button" class="btn btn-sm btn-alt-primary">{{ $value->Status }}</button></td>
                    @else 
                    <td><button type="button" class="btn btn-sm btn-alt-danger">{{ $value->Status }}</button></td>
                    @endif
                    <td>{{ $value->Version }}</td>
                    <td>{{ $value->latest_version }}</td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
@endsection