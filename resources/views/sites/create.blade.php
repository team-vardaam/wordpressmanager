@extends('layouts.backend')

@section('title', 'Add Site')

@section('content')
<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title">Add Site</h3>
    </div>
    <div class="block-content block-content-full">
        {!! Form::open(['method' => 'POST', 'route' => ['sites.store'], 'files' => true]) !!}
            <div class="row mb-3">
                <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::label('title', 'Site Title', ['class' => 'required']) !!}
                            {!! Form::text('title', '', ['placeholder' => 'Ex: Wordpress', 'class' => 'form-control form-control-alt']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('url', 'Site URL', ['class' => 'required']) !!}
                            {!! Form::text('url', '', ['placeholder' => 'Ex: http://abc/wordpress/', 'class' => 'form-control form-control-alt']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('type', 'Type', ['class' => 'required']) !!}
                            {!! Form::select('type', config('constants.site_type'), null, ['placeholder' => 'Select Site Type', 'class' => 'form-control form-control-alt']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('host', 'Host', ['class' => 'required']) !!}
                            {!! Form::select('host', config('constants.site_host'), null, ['placeholder' => 'Select Site Host', 'class' => 'form-control form-control-alt']); !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('admin', 'Administrator username', ['class' => 'required']) !!}
                            {!! Form::text('admin', '', ['placeholder' => 'Ex: John Doe', 'class' => 'form-control form-control-alt']); !!}
                        </div>

                </div>
                <div class="col-md-6">
                    <div class="form-group">
                            {!! Form::label('status', 'Status', ['class' => 'd-block']) !!}
                            @foreach (config('constants.site_status.status') as $key => $status)
                                <div class="custom-control custom-radio custom-control-inline custom-control-primary">
                                    {!! Form::radio('status', $key, ($key == config('constants.site_status.default_checked')), ['class' => 'custom-control-input', 'id' => 'status_'.$key]); !!}
                                    {!! Form::label('status_'.$key, $status, ['class' => 'custom-control-label']) !!}
                                </div>
                            @endforeach
                        </div>
                    <div class="form-group">
                        {!! Form::label('note', 'Note') !!}
                        {!! Form::textarea('note', '', ['class' => 'form-control form-control-alt', 'rows' => 9, 'placeholder' => 'Add some additinal details']) !!}
                    </div>
                </div>
            </div>
            <div class="btn-group-horizontal btn-group">
                <button type="submit" class="btn btn-primary ajax-submit"><i class="fa fa-save mr-1"></i>Save</button>
                <a href="{{ route('sites.index') }}" class="btn btn-outline-primary"><i class="fa fa-times mr-1"></i>Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>
</div>

@endsection