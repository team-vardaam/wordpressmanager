@extends('layouts.backend')
@section('title', ucfirst($title))
@section('content')
<div class="row">
    <div class="col-xl-12">
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">Active Theme</h3>
            </div>
            <div class="block-content">
                <table class="table table-borderless table-striped table-vcenter font-size-sm">
                    <tbody>
                        <tr>
                            <td style="width: 100px;">
                                <a class="font-w600" href="javascript:;">Title
                            </td>
                            <td>
                                {{ $theme->Name }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <a class="font-w600" href="javascript:;">ThemeURI
                            </td>
                            <td>
                                {{ $theme->ThemeURI }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <a class="font-w600" href="javascript:;">Author
                            </td>
                            <td>
                                {{ $theme->Author }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <a class="font-w600" href="javascript:;">AuthorURI
                            </td>
                            <td>
                                {{ $theme->AuthorURI }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <a class="font-w600" href="javascript:;">Version
                            </td>
                            <td>
                                {{ $theme->Version }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <a class="font-w600" href="javascript:;">RequiresWP
                            </td>
                            <td>
                                {{ $theme->RequiresWP }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <a class="font-w600" href="javascript:;">RequiresPHP
                            </td>
                            <td>
                                {{ $theme->RequiresPHP }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <a class="font-w600" href="javascript:;">Status
                            </td>
                            <td>
                                {{ $theme->Status }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <a class="font-w600" href="javascript:;">Description
                            </td>
                            <td>
                                {{ $theme->Description }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection