@extends('layouts.backend')
@section('title', 'Overview ('.ucfirst($title).')')
@section('content')
<div class="row row-deck">
        <div class="col-sm-6 col-xl-3">
            <div class="block block-rounded text-center d-flex flex-column">
                <div class="block-content block-content-full flex-grow-1">
                    <div class="item rounded-lg bg-body-dark mx-auto my-3">
                        <i class="fa fa-users text-muted"></i>
                    </div>
                    <div class="text-black font-size-h1 font-w700">{{ $count_info['users'] }}</div>
                    <div class="text-muted mb-3">Registered Users</div>
                </div>
                <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                    <a class="font-w500" href="{{ route('sites.user_index', $id ) }}">
                        View all users
                        <i class="fa fa-arrow-right ml-1 opacity-25"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="block block-rounded text-center d-flex flex-column">
                <div class="block-content block-content-full flex-grow-1">
                    <div class="item rounded-lg bg-body-dark mx-auto my-3">
                        <i class="fa fa-level-up-alt text-muted"></i>
                    </div>
                    <div class="text-black font-size-h1 font-w700">1</div>
                    <div class="text-muted mb-3">Active Theme</div>
                </div>
                <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                    <a class="font-w500" href="{{ route('activeTheme', $id ) }}">
                        Explore Details
                        <i class="fa fa-arrow-right ml-1 opacity-25"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="block block-rounded text-center d-flex flex-column">
                <div class="block-content block-content-full flex-grow-1">
                    <div class="item rounded-lg bg-body-dark mx-auto my-3">
                        <i class="fa fa-wrench text-muted"></i>
                    </div>
                    <div class="text-black font-size-h1 font-w700">{{ $count_info['plugins'] }}</div>
                    <div class="text-muted mb-3">Plugins</div>
                </div>
                <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                    <a class="font-w500" href="{{ route('plugins', $id ) }}">
                        View all plugins
                        <i class="fa fa-arrow-right ml-1 opacity-25"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xl-3">
            <div class="block block-rounded text-center d-flex flex-column">
                <div class="block-content block-content-full">
                    <div class="item rounded-lg bg-body-dark mx-auto my-3">
                        <i class="fa fa-comments text-muted"></i>
                    </div>
                    <div class="text-black font-size-h1 font-w700">{{ $count_info['comments'] }}</div>
                    <div class="text-muted mb-3">Spam Comments</div>
                </div>
                <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                    <a class="font-w500" href="{{ route('sites.comments.index', $id ) }}">
                        View approved comments
                        <i class="fa fa-arrow-right ml-1 opacity-25"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
    <div class="col-xl-6">
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h3 class="block-title">Site Information</h3>
            </div>
            <div class="block-content">
                <table class="table table-borderless table-striped table-vcenter font-size-sm">
                    <tbody>
                        <tr>
                            <td style="width: 200px;">
                                <a class="font-w600" href="javascript:;">WordPress Version
                            </td>
                            <td>
                                {{ $site_data->current_version }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <a class="font-w600" href="javascript:;">Latest WordPress Version
                            </td>
                            <td>
                            {{ $site_data->latest_version }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 100px;">
                                <a class="font-w600" href="javascript:;">Description
                            </td>
                            <td>
                                {{ $site_data->site_description }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection