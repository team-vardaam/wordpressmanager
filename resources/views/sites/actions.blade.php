<div class="btn-toolbar">
    <div class="btn-group">
        <a href="{{ route('siteOverview', $sites->id) }}" class="btn btn-sm btn-alt-primary js-tooltip-enabled" data-toggle="tooltip" title="Overview" data-original-title="Overview"><i class="fa fa-eye"></i>&nbsp; Overview</a>&nbsp;
        <a href="{{ route('sites.edit', $sites->id) }}" class="btn btn-sm btn-alt-warning js-tooltip-enabled" data-toggle="tooltip" title="Edit" data-original-title="Edit"><i class="fa fa-edit"></i>&nbsp; Edit</a>
    </div>
</div>