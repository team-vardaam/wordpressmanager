@extends('layouts.backend')

@section('title', ucfirst($title))

@push('styles')
<link rel="stylesheet" href="{{ asset('backoffice-asset/js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endpush

@section('content')
<div class="block block-rounded">
    <div class="block-header block-header-default">
        <h3 class="block-title">Comments List</h3>
        <div class="float-right"><a href="{{ route('sites.comments.empty-trash', $id) }}" class="btn btn-alt-danger btn-delete"><i class="fa fa-trash"></i>&nbsp;Empty Trash</a> &nbsp;<a href="{{ route('sites.comments.empty-spam', $id) }}" class="btn btn-alt-danger btn-delete"><i class="fa fa-trash"></i>&nbsp;Delete spam comments</a>&nbsp;<a href="{{ route('sites.comments.trash-spam', $id) }}" class="btn btn-alt-danger btn-delete"><i class="fa fa-trash"></i>&nbsp;Trash spam comments</a></div>
    </div>

    <div class="block-content block-content-full table-responsive">
        <table class="table table-striped table-hover table-borderless table-vcenter font-size-sm yajra-datatable">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Author</th>
                    <th>Author Email</th>
                    <th>Content</th>
                    <th>Comment Status</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('backoffice-asset/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('backoffice-asset/js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    var dTable = '';
    jQuery(function() {
        loadPartialDataTable();

        function loadPartialDataTable() {
            dTable = jQuery('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: !1,
                order: [[ 0, "desc" ]],
                language: {
                    searchPlaceholder: "Search here"
                },
                ajax: {
                    url: "{{ route('sites.comments.data', $id) }}",
                    type: "POST"
                },
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'comment_author', name: 'comment_author' },
                    { data: 'comment_author_email', name: 'comment_author_email' },
                    { data: 'comment_content', name: 'comment_content' },
                    { data: 'comment_approved', name: 'comment_approved' },
                ]   
            });
        }
    });

    function loadDataTable() {
        dTable.ajax.reload( null, false );
    }
</script>
@endpush

