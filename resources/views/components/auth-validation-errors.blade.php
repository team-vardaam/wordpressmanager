@props(['errors'])

@if ($errors->any())
    <div class="alert alert-danger" role="alert" {{ $attributes }}>
        <h4 class="alert-heading font-size-h4 my-2">{{ __('Whoops! Something went wrong.') }}</h4>
        <ul class="mb-0">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


