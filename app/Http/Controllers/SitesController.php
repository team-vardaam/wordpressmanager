<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\Sites;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;
use GuzzleHttp\Client;
use App\Helpers\Wordpress;
use App\Helpers\Wp_Sites_Manager;

class SitesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sites.index');
    }

    /**
     * get DataTable json.
     *
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        $sites = Sites::orderby('id', 'desc');

        return DataTables::of($sites)
            ->rawColumns(['plugin_status', 'actions'])
            ->editColumn('id', function ($sites) {
                return $sites->id;
            })
            ->editColumn('title', function ($sites) {
                return $sites->title;
            })
            ->editColumn('url', function ($sites) {
                return $sites->url;
            })
            ->editColumn('admin', function ($sites) {
                return $sites->admin;
            })
            ->editColumn('note', function ($sites) {
                return $sites->note;
            })
            ->editColumn('type', function ($sites) {
                return ucfirst($sites->type);
            })
            ->editColumn('host', function ($sites) {
                return ucfirst($sites->host);
            })
            ->editColumn('status', function ($sites) {
                return ucfirst($sites->status);
            })
            ->editColumn('plugin_status', function ($sites) {
                $client = new Client();
                $response = $client->request('GET', $sites->url.'/index.php/wp-json/wp_sites_manager/v1/site_details', ['http_errors' => false]);
    
                if ($code = $response->getStatusCode() == 200) {
                    return "<a href='javascript:;' data-toggle='tooltip' title='Site appears to be connected properly' data-original-title='Site appears to be connected properly'><i class='fa fa-circle fa-1x text-primary'></i></a>";
                } 
                return "<a href='javascript:;' data-toggle='tooltip' title='Site is not connected' data-original-title='Site is not connected'><i class='fa fa-circle fa-1x text-danger'></i></a>";
            })
            ->addColumn('actions', function ($sites) {
                return view('sites.actions', compact('sites'));
            })
            ->removeColumn('updated_at')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sites.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // Validations
         $request->validate([
            'title' => 'required|string|max:255',
            'url' => 'required',
            'admin' => 'required',
            'type' => 'required',
            'host' => 'required',
            'status' => [
                'required',
                Rule::in(array_keys(config('constants.site_status.status')))
            ],
        ]);

        try {
            $site = Sites::create($request->only(['title', 'url', 'admin', 'note', 'status', 'type', 'host']));
            return ['response' => 1, 'msg' => 'Site added successfully', 'redirect' => route('sites.index')];

        } catch (\Throwable $e) {
            if (config('app.env') === 'local') {
                $msg = $e->getMessage();
            } else {
                $msg = 'Failed to add site';
            }
            return ['response' => 2, 'msg' => $msg, 'redirect' => route('sites.index')];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sites = Sites::findOrFail($id);
        return view('sites.edit', compact('sites'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         // Validations
         $request->validate([
            'title' => 'required|string|max:255',
            'url' => 'required',
            'admin' => 'required',
            'type' => 'required',
            'host' => 'required',
            'status' => [
                'required',
                Rule::in(array_keys(config('constants.site_status.status')))
            ],
        ]);

        $sites = Sites::findOrFail($id);

        try {
            
            $site = $sites->fill($request->only(['title', 'url', 'admin', 'note', 'status', 'type', 'host']))->save();
            return ['response' => 1, 'msg' => 'Site updated successfully', 'redirect' => route('sites.index')];

        } catch (\Throwable $e) {
            if (config('app.env') === 'local') {
                $msg = $e->getMessage();
            } else {
                $msg = 'Failed to add site';
            }
            return ['response' => 2, 'msg' => $msg, 'redirect' => route('sites.index')];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function siteOverview($id)
    {
        try {
            $site = Sites::findOrFail($id); 
            $title = $site->title;

            $client = new Client();
            $response = $client->request('GET', $site->url.'/index.php/wp-json/wp_sites_manager/v1/site_details', ['http_errors' => false]);

            $site_data = '';
            $count_plugin = 0;
            if ($code = $response->getStatusCode() == 200) {

                // Get count information
                $link = new Wp_Sites_Manager($site->url);
                $count_info = $link->getCountInformation();

                $site_data = json_decode($response->getBody());
            } 
            else {
                return redirect(route('sites.index'));
            }

            return view('sites.site_overview', compact('title', 'id', 'site_data', 'count_info'));
        } 
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            echo $response->getStatusCode(); // check this for 404
        } 
    }

    public function activeTheme($id)
    {
        try {
            $site = Sites::findOrFail($id); 

            $client = new Client();
            $response = $client->request('GET', $site->url.'/index.php/wp-json/wp_sites_manager/v1/active_theme', ['http_errors' => false]);

            if ($code = $response->getStatusCode() == 200) {
                $theme = json_decode($response->getBody());
            } 
            else {
                return redirect(route('sites.index'));
            }
            $title = $site->title;
            return view('sites.active_theme', compact('theme', 'title'));
        } 
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            echo $response->getStatusCode(); // check this for 404
        } 
    }

    public function plugins($id)
    {     
        try {
            $plugins = null;
            $site = Sites::findOrFail($id); 

            $client = new Client();
            $response = $client->request('GET', $site->url.'/index.php/wp-json/wp_sites_manager/v1/plugins', ['http_errors' => false]);

            $plugin_list = array();
            if ($code = $response->getStatusCode() == 200) {
                $plugins = json_decode($response->getBody());

                $link = new Wordpress;

                foreach($plugins as $plugin){
                    
                    $plugin->latest_version = '';
                    
                    if(!empty($plugin->TextDomain)){

                        if($plugin->TextDomain != "wp_sites_manager"){
                            $version_info = $link->getPluginInfo($plugin->TextDomain);
                            $version = $version_info['version'];
                            $plugin->latest_version = $version;
                            $plugin_list[] = $plugin;
                        }
                    }
                }
            }
            else {
                return redirect(route('sites.index'));
            }

            $title = $site->title;
            return view('sites.plugins', compact('plugin_list', 'title'));
        } 
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            echo $response->getStatusCode(); // check this for 404
        }      
    }

    public function userIndex($id)
    {
        $site = Sites::findOrFail($id); 
        $title = $site->title;

        return view('sites.users.users', compact('title', 'id'));
    }

    public function userIndexData($id)
    {
        $site = Sites::findOrFail($id); 
        $title = $site->title;

        $client = new Client();
        $response = $client->request('GET', $site->url.'/index.php/wp-json/wp_sites_manager/v1/all_users', ['http_errors' => false]);

        if ($code = $response->getStatusCode() == 200) {
            $users = json_decode($response->getBody());

            return DataTables::of($users)
            ->editColumn('id', function ($users) {
                return $users->data->ID;
            })
            ->editColumn('display_name', function ($users) {
                return $users->data->display_name;
            })
            ->editColumn('user_login', function ($users) {
                return $users->data->user_login;
            })
            ->editColumn('user_email', function ($users) {
                return $users->data->user_email;
            })
            ->editColumn('user_url', function ($users) {
                return $users->data->user_url;
            })
            ->editColumn('user_role', function ($users) use ($site)  {
                $user_id = $users->data->ID;
                $client = new Client();
                $response = $client->request('GET', $site->url.'/index.php/wp-json/wp_sites_manager/v1/get_user_role/'.$user_id, ['http_errors' => false]);
                $role = json_decode($response->getBody());
                $users->user_role = $role;
                return ucfirst($role);
            })
            ->addColumn('actions', function ($users) use ($id) {
                return view('sites.users.actions', compact('users', 'id'));
            })
            ->make(true);
        }     
        else {
            return redirect(route('sites.index'));
        }
    }

    public function changePassword(Request $request, $site_id , $user_id)
    {
        $site = Sites::findOrFail($site_id); 

        $client = new Client();
        $response = $client->request('GET', $site->url.'/index.php/wp-json/wp_sites_manager/v1/user/'.$user_id, ['http_errors' => false]);

        if ($code = $response->getStatusCode() == 200) {
            $user = json_decode($response->getBody());
        }     
        else {
            return redirect(route('sites.index'));
        }

        if ($request->isMethod('GET')) {
            return view('sites.users.change_password', compact('user', 'site_id'));
        } else {
            $this->validatePassword($request);

            try {
                $link = new Wordpress($site->url);
                $data = ['password' => $request->password];
               $response = $link->updateUserPassword($data, $user_id);

               // $response = $client->request('POST', $site->url.'/index.php/wp-json/wp_sites_manager/v1/reset_password/'.$id.'/'.$request->password, ['http_errors' => false]);

                return ['response' => 1, 'msg' => 'Password changed successfully', 'redirect' => route('sites.user_index', $site_id)];
            } catch (\Exception $e) {
                if (config('app.env') === 'local') {
                    $msg = $e->getMessage();
                } else {
                    $msg = 'Failed to change password of user.';
                }

                return ['response' => 2, 'msg' => $msg, 'redirect' => route('sites.user_index', $site_id)];
            }
        }
    }

    /**
     * Validate Password inputs
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function validatePassword(Request $request)
    {
        return $this->validate($request, [
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
        ]);
    }

    public function deleteUser($site_id, $user_id)
    {
        try {

            $site = Sites::findOrFail($site_id); 

            $link = new Wp_Sites_Manager($site->url);
            $data = ['id' => $user_id];
            $response = $link->deleteUser($data);

            return ['response' => 1, 'msg' => 'User deleted successfully', 'redirect' => route('sites.user_index', $site_id)];
        } catch (\Exception $e) {
            if (config('app.env') === 'local') {
                $msg = $e->getMessage();
            } else {
                $msg = 'Failed to delete user.';
            }

            return ['response' => 2, 'msg' => $msg, 'redirect' => route('sites.user_index', $site_id)];
        }
    }
}
