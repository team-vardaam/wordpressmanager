<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        return view('users.index');
    }

    /**
     * get DataTable json.
     *
     * @return \Illuminate\Http\Response
     */
    public function data(Request $request)
    {
        $users = User::query();

        return DataTables::of($users)
            ->rawColumns(['name', 'email', 'actions'])
            ->editColumn('name', function ($users) {
                return $users->name;
            })
            ->editColumn('email', function ($users) {
                return '<a href="mailto:' . $users->email . '">' . $users->email . '</a>';
            })
            ->editColumn('created_at', function ($users) {
                return $users->created_at->toDayDateTimeString();
            })
            ->addColumn('actions', function ($users) {
                return view('users.actions', compact('users'));
            })
            ->make(true);
    }

    public function changePassword(Request $request, $id)
    {
        $user = User::findOrFail($id);

        if ($request->isMethod('GET')) {
            return view('users.change_password', compact('user'));
        } else {
            $this->validatePassword($request);

            try {
                $this->updatePassword($request, $user);

                return ['response' => 1, 'msg' => 'Password changed successfully', 'redirect' => route('users.index')];
            } catch (\Exception $e) {
                if (config('app.env') === 'local') {
                    $msg = $e->getMessage();
                } else {
                    $msg = 'Failed to change password of user.';
                }

                return ['response' => 2, 'msg' => $msg, 'redirect' => route('users.index')];
            }
        }
    }

    /**
     * Update password of a user
     * 
     * @param \App\User $user
     * @param Request $request
     * 
     * @return $password
     */
    public function updatePassword(Request $request, User $user)
    {
        return $user->update(['password' => Hash::make($request->password)]);
    }

    /**
     * Validate Password inputs
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function validatePassword(Request $request)
    {
        return $this->validate($request, [
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
        ]);
    }
}
