<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Helpers\Wordpress;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * 
     *
     * @param Client $client The guzzleHTTP client.
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->middleware('auth');
        $this->client = $client;
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    
    public function manageSites()
    {

        $site_list = array(
            'http://localhost/wordpress/index.php/wp-json/wp_sites_manager/v1/site_details',
            'http://localhost/wordpress-two/index.php/wp-json/wp_sites_manager/v1/site_details',
            'http://localhost/wordpress-one/index.php/wp-json/wp_sites_manager/v1/site_details'
        );

        try {

            $site_items = array();

            foreach($site_list as $site_row){

               // $client = new Client();
                $response = $this->client->request('GET', $site_row, ['http_errors' => false]);

                if ($code = $response->getStatusCode() == 200) {
                    $site_data = json_decode($response->getBody());
                    $site_items[] = $site_data;
                }
            }

            return view('manage_sites', compact('site_items'));
        } 
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            echo $response->getStatusCode(); // check this for 404
        }      
    }
}
