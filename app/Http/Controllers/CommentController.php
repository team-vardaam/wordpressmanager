<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Wp_Sites_Manager;
use Yajra\DataTables\Facades\DataTables;
Use App\Models\Sites;

class CommentController extends Controller
{
    public function index($id)
    {
        $site = Sites::findOrFail($id); 
        $title = $site->title;

        return view('sites.comments.index', compact('title', 'id'));
    }


    public function data($id)
    {
        $site = Sites::findOrFail($id); 
        $title = $site->title;

        $link = new Wp_Sites_Manager($site->url);
        $comments = $link->getCommentsList();

        return DataTables::of($comments)
        ->rawColumns(['comment_content'])
        ->editColumn('id', function ($comments) {
            return $comments['comment_ID'];
        })
        ->editColumn('comment_approved', function ($comments) {
            return "Approved";
        })
        ->make(true);
    }

    public function emptyTrashComments($site_id)
    {
        try {

            $site = Sites::findOrFail($site_id); 
            $link = new Wp_Sites_Manager($site->url);
            $data = [];
            $link->emptyTrashComments($data);
    
            return ['response' => 1, 'msg' => 'Trash deleted successfully', 'redirect' => route('sites.comments.index', $site_id)];
        } catch (\Exception $e) {
            if (config('app.env') === 'local') {
                $msg = $e->getMessage();
            } else {
                $msg = 'Failed to delete comment.';
            }

            return ['response' => 2, 'msg' => $msg, 'redirect' => route('sites.user_index', $site_id)];
        }
    }

    public function deleteSpamComments($site_id)
    {
        try {

            $site = Sites::findOrFail($site_id); 
            $link = new Wp_Sites_Manager($site->url);
            $data = [];
            $link->emptySpamComments($data);
    
            return ['response' => 1, 'msg' => 'Spam deleted successfully', 'redirect' => route('sites.comments.index', $site_id)];
        } catch (\Exception $e) {
            if (config('app.env') === 'local') {
                $msg = $e->getMessage();
            } else {
                $msg = 'Failed to delete comment.';
            }

            return ['response' => 2, 'msg' => $msg, 'redirect' => route('sites.user_index', $site_id)];
        }
    }

    public function trashSpamComments($site_id)
    {
        try {

            $site = Sites::findOrFail($site_id); 
            $link = new Wp_Sites_Manager($site->url);
            $data = [];
            $link->trashSpamComments($data);
    
            return ['response' => 1, 'msg' => 'Spam converted into trash successfully', 'redirect' => route('sites.comments.index', $site_id)];
        } catch (\Exception $e) {
            if (config('app.env') === 'local') {
                $msg = $e->getMessage();
            } else {
                $msg = 'Failed to delete comment.';
            }

            return ['response' => 2, 'msg' => $msg, 'redirect' => route('sites.user_index', $site_id)];
        }
    }
}
