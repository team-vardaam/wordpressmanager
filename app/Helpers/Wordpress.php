<?php
namespace App\Helpers;

use App\Helpers\Http;
use Carbon\Carbon;
use Illuminate\Support\Facades\Facade;

class Wordpress extends Facade
{
    const DEFAULT_BASE_URL = 'https://api.wordpress.org/';
    protected $http;

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Wordpress';
    }

    public function __construct($base_url = self::DEFAULT_BASE_URL)
    {
        $this->http = new Http($base_url);
    }

    public function getPluginInfo($slug)
    {
        $req = [
            'action' => 'plugin_information',
            'request[slug]' => $slug,
        ];
        $result = $this->http->get('plugins/info/1.2/', $req);
        return $result['response'];
    }

    public function updateUserPassword($data, $id)
    {
        return $this->http->post('index.php/wp-json/wp_sites_manager/v1/reset_password/'.$id, json_encode($data));
    }

}