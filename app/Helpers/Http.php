<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Facade;

class Http extends Facade
{
    const DEFAULT_BASE_URL = 'http://localhost';

    protected $base_url;

    public function __construct($base_url = self::DEFAULT_BASE_URL)
    {
        $this->base_url = $base_url;
    }

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Http';
    }

    protected $curl_settings = [
        CURLOPT_URL => '',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 60,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_COOKIEFILE => "",
        CURLOPT_AUTOREFERER => true,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTPHEADER => [
            "Content-Type: application/json"
        ]
    ];

    public function sendRequest($url, $req_type = 'GET', $data = '')
    {
        $r_data = [];
        $curl_settings = $this->curl_settings;

        if ($req_type == 'POST') {
            $curl_settings[CURLOPT_POSTFIELDS] = $data;
            $curl_settings[CURLOPT_URL] = $curl_settings[CURLOPT_URL].$url;
            $curl_settings[CURLOPT_CUSTOMREQUEST] = 'POST';
        } else {
            $curl_settings[CURLOPT_URL] = $curl_settings[CURLOPT_URL].$url;
            $curl_settings[CURLOPT_CUSTOMREQUEST] = 'GET';
        }
        try {
            $curl = curl_init();
            curl_setopt_array($curl, $curl_settings);
            $response = curl_exec($curl);
            $debug_info = curl_getinfo($curl);
            $err = curl_error($curl);
            curl_close($curl);
        } catch(Throwable $th){
            return $th->getMessage();
        }

        if ($err) {
            $r_data['response']['error'] = "cURL Error #:" . $err;
            $r_data['request'] = $data;
            $r_data['url'] = $curl_settings[CURLOPT_URL];
            return $r_data;
        }

        $r_data['response'] = json_decode($response, true);
        $r_data['request'] = $data;
        $r_data['url'] = $curl_settings[CURLOPT_URL];

        return $r_data;
    }

    public function setTimeout($timeoutInSec)
    {
        $this->curl_settings[CURLOPT_TIMEOUT] = $timeoutInSec;
    }

    public function addHeaders($headers)
    {
        $this->curl_settings[CURLOPT_HTTPHEADER] = array_merge($this->curl_settings[CURLOPT_HTTPHEADER], $headers);
    }

    public function get($method, $data)
    {
        $url = $this->base_url.$method.'/?'.http_build_query($data);
        return $this->sendRequest($url, 'GET');
    }

    public function post($method, $data)
    {
        return $this->sendRequest($this->base_url.$method.'/', 'POST', $data);
    }
}