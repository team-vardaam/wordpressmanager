<?php
namespace App\Helpers;

use App\Helpers\Http;
use Carbon\Carbon;
use Illuminate\Support\Facades\Facade;

class Wp_Sites_Manager extends Facade
{
    const DEFAULT_BASE_URL = 'https://api.wordpress.org/';
    protected $http;

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Wordpress';
    }

    public function __construct($base_url = self::DEFAULT_BASE_URL)
    {
        $this->http = new Http($base_url);
    }

    public function updateUserPassword($data, $id)
    {
        return $this->http->post('index.php/wp-json/wp_sites_manager/v1/reset_password/'.$id, json_encode($data));
    }

    public function getCountInformation()
    {
        $req = [];
        $result_plugin = $this->http->get('index.php/wp-json/wp_sites_manager/v1/plugins/', $req);
        $result_users = $this->http->get('index.php/wp-json/wp_sites_manager/v1/all_users/', $req);
        $result_comments = $this->http->get('index.php/wp-json/wp_sites_manager/v1/count_spam_comments/', $req);

        return array(
            'plugins' =>  count($result_plugin['response']),
            'users' => count($result_users['response']) ,
            'comments' => $result_comments['response'],
        );
    }

    public function deleteUser($data)
    {
        return $this->http->post('index.php/wp-json/wp_sites_manager/v1/delete_user/', json_encode($data));
    }

    public function getCommentsList()
    {
        $req = [];
        $result = $this->http->get('index.php/wp-json/wp_sites_manager/v1/get_comments/', $req);

        return $result['response'];
    }

    public function emptyTrashComments($data)
    {
        return $this->http->post('index.php/wp-json/wp_sites_manager/v1/empty_trash_comments/', json_encode($data));
    }

    public function emptySpamComments($data)
    {
        return $this->http->post('index.php/wp-json/wp_sites_manager/v1/empty_spam_comments/', json_encode($data));
    }

    public function trashSpamComments($data)
    {
        return $this->http->post('index.php/wp-json/wp_sites_manager/v1/trash_spam_comments/', json_encode($data));
    }

    public function getCommentStatus()
    {
        $req = [];
        $result = $this->http->get('index.php/wp-json/wp_sites_manager/v1/get_comment_status/', $req);

        return $result['response'];
    }



}