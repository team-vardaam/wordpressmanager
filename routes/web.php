<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SitesController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect('/login');   
})->middleware(['auth'])->name('welcome');

Route::get('/logged-in', function () {
     return redirect('/');   
})->middleware(['auth'])->name('logged-in');

Route::get('/home', function () {
    return view('welcome');
})->middleware(['auth'])->name('home');

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    // Sites
    Route::post('sites/data', [SitesController::class, 'data'])->name('sites.data');
    Route::resource('sites', SitesController::class);
    Route::get('/manage-sites/overview/{id}', [SitesController::class, 'siteOverview'])->name('siteOverview');

        // Active theme
        Route::get('/manage-sites/active_theme/{id}', [SitesController::class, 'activeTheme'])->name('activeTheme');

        // Plugins
        Route::get('/manage-sites/plugins/{id}', [SitesController::class, 'plugins'])->name('plugins');

        // Sites Users
        Route::post('/manage-sites/users/data/{id}', [SitesController::class, 'userIndexData'])->name('sites.user_index_data');
        Route::get('/manage-sites/users/{id}', [SitesController::class, 'userIndex'])->name('sites.user_index');
        Route::match(['GET', 'POST'], '/manage-sites/users/change-password/{site_id}/{user_id}/', [SitesController::class, 'changePassword'])->name('sites.change-password');
        Route::delete('/manage-sites/users/delete/{site_id}/{user_id}', [SitesController::class, 'deleteUser'])->name('sites.delete_user');
        

        // Sites Comments
        Route::post('/manage-sites/comments/data/{id}', [CommentController::class, 'data'])->name('sites.comments.data');
        Route::get('/manage-sites/comments/{id}', [CommentController::class, 'index'])->name('sites.comments.index');
        Route::delete('/manage-sites/empty-trash-comments/{id}', [CommentController::class, 'emptyTrashComments'])->name('sites.comments.empty-trash');
        Route::delete('/manage-sites/empty-spam-comments/{id}', [CommentController::class, 'deleteSpamComments'])->name('sites.comments.empty-spam');
        Route::delete('/manage-sites/trash-spam-comments/{id}', [CommentController::class, 'trashSpamComments'])->name('sites.comments.trash-spam');

    // User
    Route::match(['GET', 'POST'], 'users/{id}/change-password', [UserController::class, 'changePassword'])->name('users.change-password');
    Route::post('users/data', [UserController::class, 'data'])->name('users.data');
    Route::get('/user', [UserController::class, 'index'])->name('users.index');

});

